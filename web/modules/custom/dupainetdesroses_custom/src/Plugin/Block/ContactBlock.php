<?php

namespace Drupal\dupainetdesroses_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a contact block block.
 *
 * @Block(
 *   id = "dupainetdesroses_custom_contact_block",
 *   admin_label = @Translation("Contact block"),
 *   category = @Translation("DPEDR")
 * )
 */
class ContactBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#theme' => 'contact_block',
    ];
    return $build;
  }

}
