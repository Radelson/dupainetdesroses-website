<?php

namespace Drupal\dupainetdesroses_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a footerblock block.
 *
 * @Block(
 *   id = "footer_block",
 *   admin_label = @Translation("footerblock"),
 *   category = @Translation("Custom")
 * )
 */
class FooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#theme' => 'footer_block',
    ];
    return $build;
  }

}
