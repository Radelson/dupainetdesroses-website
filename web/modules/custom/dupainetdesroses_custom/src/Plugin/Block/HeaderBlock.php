<?php

namespace Drupal\dupainetdesroses_custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a headerblock block.
 *
 * @Block(
 *   id = "header_block",
 *   admin_label = @Translation("HeaderBlock"),
 *   category = @Translation("Custom")
 * )
 */
class HeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#theme' => 'header_block',
    ];
    return $build;
  }

}
