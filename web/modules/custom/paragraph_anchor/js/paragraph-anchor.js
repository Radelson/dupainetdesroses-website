(function ($, Drupal, debounce) {

  'use strict';

  /**
   * Filter webform autocomplete handler.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.menuBehavior = {
    attach: function (context) {
      $('.anchors-nav-toggle', context).once('menu-toggling')
      .each(function () {
        $(this).on('click', function() {
          $('#anchors-nav-anchor').toggleClass('toggled');
        })
      });
    }
  };

})(jQuery, Drupal, Drupal.debounce);
