module.exports = {
  prefix: 'tw-',
  important: true,
  purge: [],
  theme: {

    screens: {
      'xl-max': {'max': '1279px'},
      // => @media (max-width: 1279px) { ... }

      'lg-max': {'max': '1023px'},
      // => @media (max-width: 1023px) { ... }

      'md-max': {'max': '767px'},
      // => @media (max-width: 767px) { ... }

      'sm-max': {'max': '639px'},
      // => @media (max-width: 639px) { ... }
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    fontFamily: {
      display: ['Pecita', 'sans-serif'],
      body: ['SpaceMono', 'sans-serif'],
    },
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
      '8xl': '6rem',
    },
    extend: {
      fontFamily: {
        accent: ['Sedgwick', 'sans-serif'],
      }
    },
  },
  variants: {},
  plugins: [],
};
