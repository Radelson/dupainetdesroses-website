# Pretty much : https://github.com/geerlingguy/drupal-for-kubernetes
# Thanks Jeff !
# PHP Dependency install via Composer.
FROM composer:2 as vendor

COPY composer.json composer.json
COPY composer.lock composer.lock
COPY web/ web/

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-progress \
    --no-dev \
    --prefer-dist

# Build the Docker image for Drupal.
FROM registry.gitlab.com/radelson/drupal-php-base:latest

# Copy precompiled codebase into the container.
COPY --from=vendor /app/ /var/www/html/

# Copy other required configuration into the container.
COPY config/ /var/www/html/config/
COPY ./k8s/k8s.settings.php /var/www/html/web/sites/default/settings.php

# Make sure file ownership is correct on the document root.
RUN chown -R www-data:www-data /var/www/html/web

# Add Drush Launcher.
RUN set -ex; \
    curl -OL https://github.com/drush-ops/drush-launcher/releases/download/0.9.0/drush.phar; \
    chmod +x drush.phar; \
    mv drush.phar /usr/local/bin/drush;

# Adjust the Apache docroot.
ENV APACHE_DOCUMENT_ROOT=/var/www/html/web

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
